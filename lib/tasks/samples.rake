
namespace :test do
  namespace :unicorn do
   
    task :create_bulk_data => :environment do
      binary_string = "\xE5\xA5\xBD"*10240
      txt = " A LOREM TEXT LARGE WORD " * 512
      10000.times { Test::Unicorn::Test::HeavyTable.create( :data => binary_string, :name =>  txt, :time => Time.now.to_i ) }
    end
    
  end
end