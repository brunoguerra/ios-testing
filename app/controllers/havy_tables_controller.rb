class HavyTablesController < ApplicationController
  # GET /havy_tables
  # GET /havy_tables.json
  def index
    @havy_tables = HavyTable.paginate(:page => params[:page], :per_page => 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @havy_tables }
    end
  end

  # GET /havy_tables/1
  # GET /havy_tables/1.json
  def show
    @havy_table = HavyTable.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @havy_table }
    end
  end

  # GET /havy_tables/new
  # GET /havy_tables/new.json
  def new
    @havy_table = HavyTable.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @havy_table }
    end
  end

  # GET /havy_tables/1/edit
  def edit
    @havy_table = HavyTable.find(params[:id])
  end

  # POST /havy_tables
  # POST /havy_tables.json
  def create
    @havy_table = HavyTable.new(params[:havy_table])

    respond_to do |format|
      if @havy_table.save
        format.html { redirect_to @havy_table, notice: 'Havy table was successfully created.' }
        format.json { render json: @havy_table, status: :created, location: @havy_table }
      else
        format.html { render action: "new" }
        format.json { render json: @havy_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /havy_tables/1
  # PUT /havy_tables/1.json
  def update
    @havy_table = HavyTable.find(params[:id])
    
    respond_to do |format|
      if @havy_table.update_attributes(params[:havy_table])
        format.html { redirect_to @havy_table, notice: 'Havy table was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @havy_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /havy_tables/1
  # DELETE /havy_tables/1.json
  def destroy
    @havy_table = HavyTable.find(params[:id])
    @havy_table.destroy

    respond_to do |format|
      format.html { redirect_to havy_tables_url }
      format.json { head :no_content }
    end
  end
end
