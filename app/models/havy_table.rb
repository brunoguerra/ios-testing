class HavyTable < Test::Unicorn::Test::HeavyTable
  attr_accessible :data, :name, :time, :vocal, :content
  
  def content
    self.data
  end
  
  def content=(value)
    self.data = value
  end
end
