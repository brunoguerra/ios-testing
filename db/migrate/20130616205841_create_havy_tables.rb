class CreateHavyTables < ActiveRecord::Migration
  def change
    create_table :havy_tables do |t|
      t.string :name
      t.string :content

      t.timestamps
    end
  end
end
