class CreateTestUnicornTestHeavyTables < ActiveRecord::Migration
  def change
    create_table :test_unicorn_test_heavy_tables do |t|
      t.binary :data
      t.text :name
      t.text :vocal
      t.integer :time

      t.timestamps
    end
  end
end
