require 'test_helper'

class HavyTablesControllerTest < ActionController::TestCase
  setup do
    @havy_table = havy_tables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:havy_tables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create havy_table" do
    assert_difference('HavyTable.count') do
      post :create, havy_table: { content: @havy_table.content, name: @havy_table.name }
    end

    assert_redirected_to havy_table_path(assigns(:havy_table))
  end

  test "should show havy_table" do
    get :show, id: @havy_table
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @havy_table
    assert_response :success
  end

  test "should update havy_table" do
    put :update, id: @havy_table, havy_table: { content: @havy_table.content, name: @havy_table.name }
    assert_redirected_to havy_table_path(assigns(:havy_table))
  end

  test "should destroy havy_table" do
    assert_difference('HavyTable.count', -1) do
      delete :destroy, id: @havy_table
    end

    assert_redirected_to havy_tables_path
  end
end
